# BootLab

This is the boot lab at Glasklar.

If you want to test something related to booting on real hardware, this might be for you. We have servers sitting in a rack, connected to various equipment like switches and other servers.

We have been using this test rig for testing provisioning tools, where the opearator uses the IPMI interface in a web browser to make the BMC mount a bootable ISO with the tool on, boot it and run it.

If you need something for moving any part of ST forward, let us know and we will try to set it up.

## List of currently available resources

- [Lab network](lab-network.md)
- Supermicro server [stime](stime.md)
