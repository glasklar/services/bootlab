# Glasklar lab network

The lab network is routing the following IP networks

    91.223.231.240/29
	2001:67c:8a4:101::/64

with a router on .241 and ::1.
There's a DNS resolver on the same addresses.

There is a DHCP server for the IPv4 network with a pool of .244 to
.246. tee.sigsum.org is using .243, leaving only .242 for manual
allocation.

Use DNS name st.bootlab.glasklarteknik.se to reach a web server on
port 8000.

Hosts on the lab network can reach the internet but packets from the
internet that are not part of a flow initiated from the lab network
are being dropped.
