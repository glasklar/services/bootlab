# stime

stime.sigsum.org is a Supermicro X11SCL-F with 16GB RAM.

## ssh

The SSH server listens on port 4722.

## Serial

The RS-232 serial interface of stime is often connected to one of the
other servers in the rack, so that a sysadmin can sort things out when
link configuration has gone too bad.

## Ethernet

stime is equiped with the following NIC's.

### BMC NIC

Switch port: `vkg-sw3:GE/0/0/31`

The BMC interface is configured to be on "Dedicated LAN", VLAN 66.

It's configured with IPv4 address 192.168.66.10/24 and has no gateway
configured.

VLAN 66 can be picked up on tee.sigsum.se:enp9s0.

If you have shell on tee.sigsum.se you can reach the BMC web interface
by jumping there:

    ssh -NfL 4711:192.168.66.10:443 tee.sigsum.org
	firefox https://localhost:4711/

You should be seeing the following self-signed X.509 certificate
fingerprint:

    SHA-256 F0:F3:EA:D9:C2:CC:89:E1:5A:56:B3:E0:43:0F:79:25:75:3F:56:69:FC:77:69:9F:37:FF:6E:19:4A:BC:82:3E

After logging in as user "ADMIN" (ask someone for the password), you can
mount ISO's over SMB (yes, SMB) by

1) Navigate to Virtual Media -> CD-ROM Image and provide

```
Share Host: 192.168.66.2
Path to Image: \public\my.iso
```

2) Press Save

3) Press Mount

4) Watch the Device 1 line change from "No disk emulation set." to
"There is an iso file mounted."

ISO files are mounted from `/var/lib/sambashares/iso/` on
tee.sigsum.org. Beware of caching issues and make sure that you
unmount an ISO before updating it on tee.

### On board NIC #1
Driver: TBD

MAC: `3c:ec:ef:29:60:2a`
Interface: `eno1` or `eth1`

Switch port: `vkg-sw3:GE/0/0/32` on VLAN#67.

### On board NIC #2
Driver: TBD

MAC: `3c:ec:ef:29:60:2b`
Interface: `eno2` or `eth3`

Switch port: `vkg-sw3:GE/0/0/30` part of the [lab network](./lab-network.md).
Can be picked up on ens1 in tee.sigsum.org.

### Dual NIC card

Driver: tg3?
Bonded into a trunk port on the [lab network](./lab-network.md) which has a DHCP server.

#### Dual NIC port #1

MAC: `00:0a:f7:2a:59:bc`
Interface: `enp1s0f0` or `eth0`

Switch port: `vkg-sw3:GE/0/0/4` is bonded into vkg-sw3:Eth-Trunk1, LACP mode, part of the [lab network](./lab-network.md).

#### Dual NIC port #2

MAC: `00:0a:f7:2a:59:bd`
Interface: `enp1s0f1` or `eth2`

Switch port: `vkg-sw3:GE/0/0/2` is bonded into vkg-sw3:Eth-Trunk1, LACP mode, part of the [lab network](./lab-network.md).
